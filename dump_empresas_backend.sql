﻿--
-- PostgreSQL database dump
--

-- Dumped from database version 11.6
-- Dumped by pg_dump version 11.6

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: empresas_backend; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA empresas_backend;


ALTER SCHEMA empresas_backend OWNER TO postgres;

--
-- Name: uuid_generate(); Type: FUNCTION; Schema: empresas_backend; Owner: postgres
--

CREATE FUNCTION empresas_backend.uuid_generate() RETURNS uuid
    LANGUAGE plpgsql
    AS $$DECLARE
uuid_result UUID;
BEGIN
	SELECT uuid_in(md5(random()::text || clock_timestamp()::text)::cstring)
	INTO uuid_result;

	RETURN uuid_result;
END;$$;


ALTER FUNCTION empresas_backend.uuid_generate() OWNER TO postgres;

--
-- Name: enterprise_types_seq; Type: SEQUENCE; Schema: empresas_backend; Owner: postgres
--

CREATE SEQUENCE empresas_backend.enterprise_types_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9999999
    CACHE 1;


ALTER TABLE empresas_backend.enterprise_types_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: enterprise_types; Type: TABLE; Schema: empresas_backend; Owner: postgres
--

CREATE TABLE empresas_backend.enterprise_types (
    id uuid DEFAULT empresas_backend.uuid_generate() NOT NULL,
    enterprise_type_code integer DEFAULT nextval('empresas_backend.enterprise_types_seq'::regclass) NOT NULL,
    name character varying(120) NOT NULL
);


ALTER TABLE empresas_backend.enterprise_types OWNER TO postgres;

--
-- Name: enterprises; Type: TABLE; Schema: empresas_backend; Owner: postgres
--

CREATE TABLE empresas_backend.enterprises (
    id uuid DEFAULT empresas_backend.uuid_generate() NOT NULL,
    enterprise_type_code integer NOT NULL,
    name character varying(180) NOT NULL,
    cnpj character varying(14)
);


ALTER TABLE empresas_backend.enterprises OWNER TO postgres;

--
-- Name: enterprises_seq; Type: SEQUENCE; Schema: empresas_backend; Owner: postgres
--

CREATE SEQUENCE empresas_backend.enterprises_seq
    START WITH 1
    INCREMENT BY 1
    MINVALUE 0
    MAXVALUE 9999999
    CACHE 9999999;


ALTER TABLE empresas_backend.enterprises_seq OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: empresas_backend; Owner: postgres
--

CREATE TABLE empresas_backend.users (
    id uuid DEFAULT empresas_backend.uuid_generate() NOT NULL,
    email character varying(120) NOT NULL,
    password character varying(120) NOT NULL
);


ALTER TABLE empresas_backend.users OWNER TO postgres;

--
-- Data for Name: enterprise_types; Type: TABLE DATA; Schema: empresas_backend; Owner: postgres
--

COPY empresas_backend.enterprise_types (id, enterprise_type_code, name) FROM stdin;
a655e9e3-652e-5c55-a946-806627b875aa	1	Agro
09ef44a3-2986-29de-f049-e4e4b676049e	2	Aviation
6ffef80f-e3b7-01ff-8c32-643878be2e6f	3	Biotech
783a2e95-4e35-303a-206c-0cf3a6b40c52	4	Eco
9eab8e04-df49-6bca-de1c-d3717b97a56e	5	Ecommerce
ba3a33a1-3115-a69c-dee5-ad7e908b0590	6	Education
0d64701a-571d-1285-bacf-7c01c68c50a2	7	Fashion
6b79b5aa-f150-0a6c-2403-747c23b625b2	8	Fintech
029bd3da-f1d1-2c73-e248-bf7b92534969	9	Food
0f3f92ad-ed08-a73c-b349-d62bfdc50837	10	Games
a045be8f-c710-19f7-3d4a-a4a5d05b0a2d	11	Health
96d56184-0852-3bf2-0507-b5fbe5b3eed0	12	IOT
0b1773a0-e776-da58-456c-de0cfcfae30a	13	Logistics
9a98cab9-e693-9908-628d-1c64f4eb28ca	14	Media
36a8ff87-d1eb-826a-f69e-c7ba89aeef1c	15	Mining
bd9710b8-cf9f-76fc-1ee8-dea3f4410083	16	Products
05a12418-f6f1-af8b-572f-d28ae6bcd62f	17	Real Estate
42faead2-49f5-cda1-5901-95bdca44e8ac	18	Service
5e1bce0e-ae39-8e77-8975-d83faf382528	19	Smart City
3c4143d1-2da1-2419-2ceb-9d03a04c0162	20	Social
c4b4c816-bbb6-65db-bad0-a511f3850b05	21	Software
5e3d40bf-55d8-41fa-0153-92e1ebb91caf	22	Technology
ff6021e3-fe94-d046-d609-a2f4cc2390d2	23	Tourism
78627e3d-436e-9ef6-0d35-bf2ab4fc4788	24	Transport
\.


--
-- Data for Name: enterprises; Type: TABLE DATA; Schema: empresas_backend; Owner: postgres
--

COPY empresas_backend.enterprises (id, enterprise_type_code, name, cnpj) FROM stdin;
83e5c4f3-2a0a-f830-13d3-becffb4b5923	2	Empresa2	13856403000189
b426a65b-2d1e-eed9-5d59-214801e0aa78	4	Empresa4	46725473000157
2ca54f7d-e043-7481-565f-d2e1b02eed18	6	Empresa6	48486575000192
7cb2c810-e6ae-d825-0794-d6797e3623e6	8	Empresa8	15765606000195
d18472c9-1369-ec12-7f95-a4deef716947	10	Empresa10	81586863000194
7ca1eb0f-74c3-ca8b-6660-714c2cf6cc48	12	Empresa12	17947323000190
6e703d55-8ff4-abc8-a807-03d12d1da0a6	14	Empresa14	92471173000106
d13e4c52-aa8d-e62e-0fd7-35562454486d	16	Empresa16	72176098000143
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: empresas_backend; Owner: postgres
--

COPY empresas_backend.users (id, email, password) FROM stdin;
02d48584-6c70-124d-de52-59144d83a07e	testeapple2@ioasys.com.br	ed2b1f468c5f915f3f1cf75d7068baae
7581ebf8-8c1c-c96b-a1fb-43408dda2d30	testeapple3@ioasys.com.br	ed2b1f468c5f915f3f1cf75d7068baae
53214680-d2f5-8dcd-6e06-055259f5cdae	testeapple4@ioasys.com.br	ed2b1f468c5f915f3f1cf75d7068baae
97b720ba-e663-cad4-8b24-180200d50164	testeapple5@ioasys.com.br	ed2b1f468c5f915f3f1cf75d7068baae
25de87e7-8eeb-444a-3c3d-5a0f5109ceec	testeapple6@ioasys.com.br	ed2b1f468c5f915f3f1cf75d7068baae
1f4259e5-8d3d-7742-e998-40e4efc9ba17	testeapple7@ioasys.com.br	ed2b1f468c5f915f3f1cf75d7068baae
\.


--
-- Name: enterprise_types_seq; Type: SEQUENCE SET; Schema: empresas_backend; Owner: postgres
--

SELECT pg_catalog.setval('empresas_backend.enterprise_types_seq', 24, true);


--
-- Name: enterprises_seq; Type: SEQUENCE SET; Schema: empresas_backend; Owner: postgres
--

SELECT pg_catalog.setval('empresas_backend.enterprises_seq', 1, false);


--
-- Name: users ID_PK; Type: CONSTRAINT; Schema: empresas_backend; Owner: postgres
--

ALTER TABLE ONLY empresas_backend.users
    ADD CONSTRAINT "ID_PK" PRIMARY KEY (id);


--
-- Name: enterprise_types ID_enterprise_type_code_PK; Type: CONSTRAINT; Schema: empresas_backend; Owner: postgres
--

ALTER TABLE ONLY empresas_backend.enterprise_types
    ADD CONSTRAINT "ID_enterprise_type_code_PK" PRIMARY KEY (id);


--
-- Name: enterprises ID_enterprises_code_PK; Type: CONSTRAINT; Schema: empresas_backend; Owner: postgres
--

ALTER TABLE ONLY empresas_backend.enterprises
    ADD CONSTRAINT "ID_enterprises_code_PK" PRIMARY KEY (id);


--
-- Name: users email_unique; Type: CONSTRAINT; Schema: empresas_backend; Owner: postgres
--

ALTER TABLE ONLY empresas_backend.users
    ADD CONSTRAINT email_unique UNIQUE (email);


--
-- Name: enterprise_types enteprise_types_unique; Type: CONSTRAINT; Schema: empresas_backend; Owner: postgres
--

ALTER TABLE ONLY empresas_backend.enterprise_types
    ADD CONSTRAINT enteprise_types_unique UNIQUE (enterprise_type_code);


--
-- Name: enterprises name_enterprises_unique; Type: CONSTRAINT; Schema: empresas_backend; Owner: postgres
--

ALTER TABLE ONLY empresas_backend.enterprises
    ADD CONSTRAINT name_enterprises_unique UNIQUE (name);


--
-- Name: enterprise_types name_unique; Type: CONSTRAINT; Schema: empresas_backend; Owner: postgres
--

ALTER TABLE ONLY empresas_backend.enterprise_types
    ADD CONSTRAINT name_unique UNIQUE (name);


--
-- Name: FUNCTION uuid_generate(); Type: ACL; Schema: empresas_backend; Owner: postgres
--

REVOKE ALL ON FUNCTION empresas_backend.uuid_generate() FROM PUBLIC;


--
-- PostgreSQL database dump complete
--

