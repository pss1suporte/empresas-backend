/**
 * EnterprisesController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
  index: function (req, res) {

  	var myQuery = "select * from empresas_backend.enterprises";

    Enterprises.getDatastore().sendNativeQuery(myQuery, function (err, enterprises){
      if(err || !enterprises.rows.length){
        return res.json({"status": 0, "error": err});
      }
      else{
        return res.json(enterprises);
      }
    });
  	
  },

  findByTypeAndName: function (req, res) {

    var name = req.params['name'],
        enterprise_types = req.params['enterprise_types'],
        myQuery = "select * from empresas_backend.enterprises where name like '%" + name + "%' and  enterprise_type_code = '" + enterprise_types + "'" ;
  	
  	Enterprises.getDatastore().sendNativeQuery(myQuery, function (err, enterprises){
      if(err || !enterprises.rows.length){
        return res.json({"status": 0, "error": err});
      }
      else{
        return res.json(enterprises);
      }
    });
  },

  findById: function (req, res) {

  	var id = req.params['id'],
        myQuery = "select * from empresas_backend.enterprises where id = '" + id + "'";

	Enterprises.getDatastore().sendNativeQuery(myQuery, function (err, enterprises){
      if(err || !enterprises.rows.length){
        return res.json({"status": 0, "error": err});
      }
      else{
        return res.json(enterprises);
      }
    });
  },

};