/**
 * Enterprises.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  tableName: 'enterprises',
  
  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
    id: {
      type: 'number',
      columnName: 'id',
      columnType: 'integer',
      unique: true,
      required: true
    },
    uuid: {
      type: 'string',
      columnName: 'uuid',
      columnType: 'uuid',
      unique: true,
      required: true
    },
    enterprise_type_code: {
      type: 'number',
      columnName: 'enterprise_type_code',
      columnType: 'integer',
      unique: true,
      required: true
    },
    name: {
      type: 'string',
      columnName: 'name',
      columnType: 'character varying(180)',
      unique: true,
      required: true
    },
    cnpj: {
      type: 'string',
      columnName: 'cnpj',
      columnType: 'character varying(14)',
      allowNull: true
    },


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

  },

};

