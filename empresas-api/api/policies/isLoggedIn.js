/**
 * Policy Mappings
 * (api/policies)
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

 module.exports = function isLoggedIn (req, res, next) {


    /*
      Implementar aqui a integração com o JWT/Auth2.0 
    */

    if (typeof req.session.User != "undefined") {
      
      console.log(req.session);

        User.findOne(req.session.User.id).populate('roles').exec(function(err,user){
           if(err) return res.forbidden('You are not permitted to perform this action.');
           if(!user) return res.redirect('/user/new');
           for(var i in user.roles){
              if(user.roles[i]['name'] == 'ROLE_ADMIN'){
                  return next();
              }
           }
           return res.redirect('/user/show/'+req.session.User.id);
        });

    } else {
        //return res.redirect('/session/new');
        return res.send('Acesso negado! Veja a politica: isLoggedIn');
    }
};